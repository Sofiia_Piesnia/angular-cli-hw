import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthenticationService } from './services/authentication.service';

@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  imports: [CommonModule],
  providers: [AuthenticationService],
  exports: [HeaderComponent, FooterComponent],
})
export class CoreModule {}
